using System;
using Machine.Specifications;
using Robot.Infrastructure.Arena;
using Robot.Infrastructure.Robot;
using Robot.Presentation;
using Robot.Presentation.Parser;
using Robot.Presentation.RobotInstructor;
using Robot.Tasks.Arena;
using Robot.Tasks.Robot;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Acceptance.Tests 
{
    public abstract class acceptance_tests : Observes<Program>
    {
        private Establish c = () =>
            {
                output = string.Empty;
                sut_factory.create_using(
                    () =>
                    new Program(new DefaultConsole(),
                                new RobotInstructor(new ArenaTasks(new ArenaRepository()),
                                                    new InputParser(),
                                                    new RobotHandler(new RobotTasks(new ArenaRepository(), new RobotRepository())))));
                args = new[]{"5 5", "3 3 N LMLMLMLMM"};   
            };

        protected static string[] args;
        protected static ExitCode result;
        protected static string output;
    }

    public class when_given_a_valid_input : acceptance_tests
    {
        private Because b = () => result = sut.Run(args, out output);

        private It should_return_robot_position = () =>
            {
                Console.WriteLine(output);
                output.ShouldEqual("3 4 N" + Environment.NewLine);
            };
    }

    public class when_given_the_sample_input : acceptance_tests
    {
        private Establish c = () => { args = new string[] {"5 5", "1 2 N LMLMLMLMM", "3 3 E MMRMMRMRRM"}; };

        private Because b = () => result = sut.Run(args, out output);

        private It should_return_1_3_N_and_5_1_E =
            () =>
                {
		    Console.Write(output);
                    output.ShouldEqual("1 3 N" + Environment.NewLine + "5 1 E" + Environment.NewLine);
                };
    }

    public class when_robot_is_moved_outside_of_arena : acceptance_tests
    {
        private Establish c = () => { args = new string[] {"2 2", "-1 0 N LMLMLMLM"}; };

        private Because b = () => result = sut.Run(args, out output);

        private It should_display_warning_message = () =>
            {
		Console.Write(output);
		output.ShouldContain("Robot is outside the boundaries of arena");
            };
    }

    public class when_given_an_alphabet_input : acceptance_tests
    {
        private Establish c = () => { args = new string[] {"a b"}; };

        private Because b = () => spec.catch_exception(() => sut.Run(args, out output));

        private It should_throw_exception = () => spec.exception_thrown.ShouldBeOfType<FormatException>();
    }

    public class when_given_invalid_numeric_coordinates_for_arena : acceptance_tests
    {
        private Establish c = () => { args = new string[] {"5 5 5", "3 3 N LMLMLMLM"}; };

        private Because b = () => spec.catch_exception(() => sut.Run(args, out output));

        private It should_throw_argument_exception = () => spec.exception_thrown.ShouldBeOfType<ArgumentException>();
    }

    public class when_given_invalid_data_for_robot : acceptance_tests
    {
        private Establish c = () => { args = new string[] { "5 5 5", "3 3 NLMLMLMLM" }; };

        private Because b = () => spec.catch_exception(() => sut.Run(args, out output));

        private It should_throw_argument_exception = () => spec.exception_thrown.ShouldBeOfType<ArgumentException>();
    }


}
