﻿using System;
using Robot.Framework;

namespace Robot.Domain
{
    public class Heading : SmartEnum<Heading>
    {
        public string LeftHeading { get; set; }
        public string RightHeading { get; set; }
        public Action<Robot> RobotMove { get; set; }
        public static Heading North = new Heading("N", "W", "E", (robot) => robot.Location.PointY += 1);

        public static Heading East = new Heading("E", "N", "S", (robot) => robot.Location.PointX += 1);

        public static Heading South = new Heading("S", "E", "W", (robot) => robot.Location.PointY -= 1);

        public static Heading West = new Heading("W", "S", "N", (robot) => robot.Location.PointX -= 1);

        private Heading(string key, string leftHeading, string rightHeading, Action<Robot> robotMove)
        {
            LeftHeading = leftHeading;
            RightHeading = rightHeading;
            RobotMove = robotMove;
            Key = key.ToLower();
            SmartEnums.Add(key.ToLower(), this);
        }
    }
}