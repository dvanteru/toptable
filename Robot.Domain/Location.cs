﻿namespace Robot.Domain
{
    public class Location
    {
        public int PointX { get; set; }
        public int PointY { get; set; }
    }
}