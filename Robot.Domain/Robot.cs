﻿namespace Robot.Domain
{
    public class Robot
    {
        public Location Location { get; set; }

        public Heading Heading { get; set; }
    }
}
