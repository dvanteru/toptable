﻿using Robot.Framework;

namespace Robot.Domain 
{
    public class SpinDirection : SmartEnum<SpinDirection>
    {
	public static SpinDirection Left = new SpinDirection("L");

	public static SpinDirection Right = new SpinDirection("R");

        private SpinDirection(string key)
        {
            Key = key.ToLower();
	    SmartEnums.Add(key.ToLower(), this);
        }
    }
}
