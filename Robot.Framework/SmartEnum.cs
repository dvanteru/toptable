﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Robot.Framework 
{   
    public class SmartEnum : ISmartEnum
    {
        public string Key { get; protected internal set; }
    }

    public class SmartEnum<T> : SmartEnum where T : SmartEnum<T> 
    {
        protected static readonly Dictionary<string, T> SmartEnums = new Dictionary<string, T>();
    

        protected void Add(string key, T item) {
            Key = key;
            SmartEnums.Add(key.ToLower(), item);
        }

        protected static T Get(string key)            
        {
            if (key == null) {
                return null;
            }
            T t;
            SmartEnums.TryGetValue(key.ToLower(), out t);
            return t;
        }

        public static T GetFor(string key) 
        {
            EnsureValues();
            return Get(key);
        }

        private static void EnsureValues() {
            if (SmartEnums.Count != 0) {
                return;
            }
            var fieldInfos = typeof(T).GetFields(BindingFlags.Static | BindingFlags.Public);
            fieldInfos[0].GetValue(null);
        }
    }
}