using System;
using Robot.Domain;
using Robot.Infrastructure.Arena.Contract;

namespace Robot.Infrastructure.Arena
{
    public class ArenaRepository : IArenaRepository
    {
        private static Domain.Arena arena;

        public Domain.Arena Create()
        {
            return arena ?? (arena = new Domain.Arena());
        }

        public Domain.Arena Get()
        {
            return arena ?? Create();
        }

        public Domain.Arena Update(Domain.Arena updatedArena)
        {
            if (arena == null) arena = Create();
	    arena.Location = new Location {PointX = updatedArena.Location.PointX, PointY = updatedArena.Location.PointY};
            return arena;
        }
    }
}