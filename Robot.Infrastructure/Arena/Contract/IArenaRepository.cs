namespace Robot.Infrastructure.Arena.Contract
{
    public interface IArenaRepository
    {
        Domain.Arena Create();
        Domain.Arena Get();
        Domain.Arena Update(Domain.Arena arena);
    }
}