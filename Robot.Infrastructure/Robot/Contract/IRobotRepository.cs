namespace Robot.Infrastructure.Robot.Contract
{
    public interface IRobotRepository
    {
        Domain.Robot Create();
        Domain.Robot Get();
        Domain.Robot Update(Domain.Robot robot);
    }
}