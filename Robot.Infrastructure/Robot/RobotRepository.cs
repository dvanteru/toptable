using Robot.Domain;
using Robot.Infrastructure.Robot.Contract;

namespace Robot.Infrastructure.Robot
{
    public class RobotRepository : IRobotRepository
    {
        private static Domain.Robot robot;

        public Domain.Robot Create()
        {
            return robot ?? (robot = new Domain.Robot());
        }

        public Domain.Robot Get()
        {
            return robot ?? Create();
        }

        public Domain.Robot Update(Domain.Robot updatedRobot)
        {
            if (robot == null) Create();

            robot.Location = new Location
                {
                    PointX = updatedRobot.Location.PointX,
                    PointY = updatedRobot.Location.PointY
                };
            robot.Heading = updatedRobot.Heading;

            return robot;
        }
    }
}