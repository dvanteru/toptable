using System;
using Robot.Presentation.RobotInstructor.ViewModel;

namespace Robot.Presentation.Extensions
{
    public static class InputExtensions
    {
        public static Coordinates ExtractArena(this string[] input)
        {
            if (input[0].Split(' ').Length != 2) throw new ArgumentException("Wrong parameters for Arena");
            var pointX = int.Parse(input[0].Split(' ')[0]);
            var pointY = int.Parse(input[0].Split(' ')[1]);
            return new Coordinates { PointX = pointX, PointY = pointY };
        }

	public static RobotInstruction MapToRobotInstruction(this string robotInput)
	{
	    var inputSplit = robotInput.Split(' ');
	    if (inputSplit.Length != 4) throw new ArgumentException("Robot argument should be like 3 3 N LMLMLMLML");
	    if (!inputSplit[3].AreValidSpinAndMoves()) throw new ArgumentException("Unrecognised moves for robot");
		return new RobotInstruction
		    {
		        Heading = inputSplit[2],
			RobotCoordinates = new Coordinates
			    {
			        PointX = int.Parse(inputSplit[0]),
				PointY = int.Parse(inputSplit[1])
			    },
			SpinAndMoves = inputSplit[3]
		    };
	}
    }
}