﻿namespace Robot.Presentation.Extensions
{
    public static class StringExtensions
    {
        public static string FormatString(this Domain.Robot robot)
        {
            return string.Format("{0} {1} {2}", robot.Location.PointX, robot.Location.PointY,
                                 robot.Heading.Key.ToUpper());
        }
    }
}