using System.Linq;
using Robot.Framework;
using Robot.Presentation.RobotConsole;

namespace Robot.Presentation.Extensions
{
    static internal class ValidationExtensions
    {
        public static bool AreValidSpinAndMoves(this string moves)
        {
            var isValid = false;
            foreach (var move in moves)
            {
                if (SmartEnum<NavigationConsole>.GetFor(move.ToString()) == null)
                {
                    isValid = false;
                    break;
                }
               
                isValid = true;
            }
            return isValid;
        }

        public static bool IsInArena(this Domain.Robot robot, Domain.Arena arena)
        {
            if (robot.Location.PointX > arena.Location.PointX || robot.Location.PointY > arena.Location.PointY ||
                robot.Location.PointX < 0 || robot.Location.PointY < 0)
                return false;
            else
                return true;
        }
    }
}