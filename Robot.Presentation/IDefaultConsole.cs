﻿namespace Robot.Presentation
{
    public interface IDefaultConsole
    {
        void Write(string message);

        void WriteLine(string message);

        string ReadLine();
    }
}