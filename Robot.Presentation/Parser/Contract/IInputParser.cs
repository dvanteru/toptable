using Robot.Presentation.RobotInstructor.ViewModel;

namespace Robot.Presentation.Parser.Contract
{
    public interface IInputParser
    {
        RobotInputViewModel Parse(string[] input);
    }
}