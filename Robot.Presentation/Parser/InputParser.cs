using System.Linq;
using Robot.Presentation.Extensions;
using Robot.Presentation.Parser.Contract;
using Robot.Presentation.RobotInstructor.ViewModel;

namespace Robot.Presentation.Parser 
{
    public class InputParser : IInputParser 
    {
        public RobotInputViewModel Parse(string[] input)
        {
            var robotInputViewModel = new RobotInputViewModel { ArenaCoordinates = input.ExtractArena() };

            var robotInputs = input.Skip(1);
            foreach (var robotInput in robotInputs)
                robotInputViewModel.RobotInstructions.Add(robotInput.MapToRobotInstruction());

            return robotInputViewModel;
        }
    }
}
