﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Presentation.Parser.Contract;
using Robot.Presentation.RobotInstructor.Contract;

namespace Robot.Presentation 
{
    public class Program
    {
        private readonly IDefaultConsole defaultConsole;
        private readonly IRobotInstructor robotInstructor;

        public Program(IDefaultConsole defaultConsole, IRobotInstructor robotInstructor)
        {
            this.defaultConsole = defaultConsole;
            this.robotInstructor = robotInstructor;
        }

        public static void Main(string[] args)
        {
            var program = ProgramBuilder.Build(args);
            string output;
            var exitCode = program == null ? ExitCode.InitializationFailure : program.Run(args, out output);
	    Environment.Exit((int)exitCode);
        }

        public ExitCode Run(string[] args, out string robotPostion)
        {
            var currentPostions = this.robotInstructor.Instruct(args);
            robotPostion = currentPostions.FormatOutput();
	    this.defaultConsole.Write(robotPostion);
	    return ExitCode.Success;
        }
    }

    public static class OutputExtensions
    {
        public static string FormatOutput(this List<string> input)
        {
           return input.Aggregate(string.Empty, (current, position) => current + position + Environment.NewLine);
        }
    }
}

