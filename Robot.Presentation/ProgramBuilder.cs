﻿using Robot.Infrastructure.Arena;
using Robot.Infrastructure.Robot;
using Robot.Presentation.Parser;
using Robot.Presentation.RobotInstructor;
using Robot.Tasks.Arena;
using Robot.Tasks.Robot;

namespace Robot.Presentation
{
    public static class ProgramBuilder
    {
        public static Program Build(string[] args)
        {
            var defaultConsole = new DefaultConsole();
            var robotInstructor = new RobotInstructor.RobotInstructor(new ArenaTasks(new ArenaRepository()),
						      new InputParser(), 
                                                      new RobotHandler(new RobotTasks(new ArenaRepository(), new RobotRepository())));
            return new Program(defaultConsole, robotInstructor);
        }
    }
}