using System;
using Robot.Framework;
using Robot.Tasks.Robot.Contract;

namespace Robot.Presentation.RobotConsole
{
    public class NavigationConsole : SmartEnum<NavigationConsole>
    {
        public Action<IRobotTasks> SpinOrMoveAction { get; set; }

        public static NavigationConsole Left = new NavigationConsole("L", (robotTasks) => robotTasks.Spin("L"));

        public static NavigationConsole Right = new NavigationConsole("R", (robotTasks) => robotTasks.Spin("R"));

        public static NavigationConsole Move = new NavigationConsole("M", (robotTasks) => robotTasks.Move());

        private NavigationConsole(string key, Action<IRobotTasks> spinOrMoveAction)
        {
            SpinOrMoveAction = spinOrMoveAction;
            Key = key.ToLower();
            SmartEnums.Add(key.ToLower(), this);
        }
    }
}