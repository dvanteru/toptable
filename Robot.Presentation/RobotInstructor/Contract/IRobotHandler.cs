﻿using System.Collections.Generic;
using Robot.Domain;
using Robot.Presentation.RobotInstructor.ViewModel;

namespace Robot.Presentation.RobotInstructor.Contract
{
    public interface IRobotHandler
    {
        void Handle(RobotInputViewModel robotViewModel, Arena arena, out List<string> robotPositions);
    }
}