﻿using System.Collections.Generic;

namespace Robot.Presentation.RobotInstructor.Contract
{
    public interface IRobotInstructor
    {
        List<string> Instruct(string[] args);
    }
}