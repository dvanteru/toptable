﻿using System.Collections.Generic;
using Robot.Domain;
using Robot.Presentation.Extensions;
using Robot.Presentation.RobotConsole;
using Robot.Presentation.RobotInstructor.Contract;
using Robot.Presentation.RobotInstructor.ViewModel;
using Robot.Tasks.Robot.Contract;

namespace Robot.Presentation.RobotInstructor
{
    public class RobotHandler : IRobotHandler
    {
        private readonly IRobotTasks robotTasks;

        public RobotHandler(IRobotTasks robotTasks)
        {
            this.robotTasks = robotTasks;
        }

        public void Handle(RobotInputViewModel robotViewModel, Arena arena, out List<string> robotPositions)
        {
	    robotPositions = new List<string>();
            foreach (var robotInstruction in robotViewModel.RobotInstructions)
            {
                var robot = robotTasks.DeployRobot(robotInstruction.RobotCoordinates.PointX.Value,
                                                   robotInstruction.RobotCoordinates.PointY.Value, robotInstruction.Heading);
                ValidateIfRobotIsInArena(robot, arena, robotPositions);
                foreach (var spinAndMove in robotInstruction.SpinAndMoves)
                {
                    NavigationConsole.GetFor(spinAndMove.ToString().ToLower()).SpinOrMoveAction.Invoke(this.robotTasks);
                }
                
                ValidateIfRobotIsInArena(robot, arena, robotPositions);
                robotPositions.Add(robot.FormatString());
            }
        }

        private static void ValidateIfRobotIsInArena(Domain.Robot robot, Arena arena, List<string> robotPositions)
        {
            if (!robot.IsInArena(arena))
                robotPositions.Add("Warning: Robot is outside the boundaries of arena");
        }
    }
}