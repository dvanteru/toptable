﻿using System.Collections.Generic;
using Robot.Presentation.Parser.Contract;
using Robot.Presentation.RobotInstructor.Contract;
using Robot.Tasks.Arena.Contract;

namespace Robot.Presentation.RobotInstructor
{
    public class RobotInstructor : IRobotInstructor
    {
        private readonly IArenaTasks arenaTasks;
        private readonly IInputParser inputParser;
        private readonly IRobotHandler robotHandler;

        public RobotInstructor(IArenaTasks arenaTasks, IInputParser inputParser, IRobotHandler robotHandler)
        {
            this.arenaTasks = arenaTasks;
            this.inputParser = inputParser;
            this.robotHandler = robotHandler;
        }

        public List<string> Instruct(string[] args)
        {
            var robotViewModel = this.inputParser.Parse(args);
            var robotPositions = new List<string>();
            var arena = this.arenaTasks.CreateArena(robotViewModel.ArenaCoordinates.PointX.Value,
                                                    robotViewModel.ArenaCoordinates.PointY.Value);
            robotHandler.Handle(robotViewModel, arena, out robotPositions);
            return robotPositions;
        }
    }
}