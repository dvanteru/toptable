namespace Robot.Presentation.RobotInstructor.ViewModel
{
    public class Coordinates
    {
        public int? PointX { get; set; }
        public int? PointY { get; set; }
    }
}