using System.Collections.Generic;

namespace Robot.Presentation.RobotInstructor.ViewModel
{
    public class RobotInputViewModel
    {
        public RobotInputViewModel()
        {
            ArenaCoordinates = new Coordinates();
            this.RobotInstructions = new List<RobotInstruction>();
        }
        public Coordinates ArenaCoordinates { get; set; }
        public IList<RobotInstruction> RobotInstructions { get; set; }
    }
}