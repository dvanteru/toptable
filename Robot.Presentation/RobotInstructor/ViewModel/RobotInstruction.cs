namespace Robot.Presentation.RobotInstructor.ViewModel
{
    public class RobotInstruction
    {
        public Coordinates RobotCoordinates { get; set; }
        public string Heading { get; set; }
        public string SpinAndMoves { get; set; }
    }
}