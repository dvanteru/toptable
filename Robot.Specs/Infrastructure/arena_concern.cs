using Machine.Specifications;
using Robot.Domain;
using Robot.Infrastructure.Arena;
using Robot.Infrastructure.Arena.Contract;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Specs.Infrastructure
{
    [Subject("Arena updates concern")]
    public abstract class arena_concern : Observes<IArenaRepository, ArenaRepository>
    {
        protected static Domain.Arena result;
    }

    public class when_asked_to_create_arena : arena_concern
    {
        private Because b = () => result = sut.Create();

        private It should_create_arena = () => result.ShouldNotBeNull();
    }

    public class when_asked_to_get_existing_arena : arena_concern
    {
        private static Domain.Arena arena;

        private Because b = () => arena = sut.Get();

        private It should_return_existing_arena = () => arena.ShouldNotBeNull();
    }

    public class when_asked_to_update_arena_location : arena_concern
    {
        private Establish c = () =>
            {
		arena = new Arena{ Location = new Location{ PointX = 5, PointY = 5} };
            };

        private Because b = () => result = sut.Update(arena);

        private It should_add_location_to_arena = () => result.Location.ShouldNotBeNull();

        private It should_create_location_at_point_x = () => result.Location.PointX.ShouldEqual(5);

        private static Arena arena;
    }
}
