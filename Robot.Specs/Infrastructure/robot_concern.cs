using Machine.Specifications;
using Robot.Domain;
using Robot.Infrastructure.Robot;
using Robot.Infrastructure.Robot.Contract;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Specs.Infrastructure 
{
    [Subject("Robot updates concern")]
    public abstract class robot_concern : Observes<IRobotRepository, RobotRepository>
    {
        protected static Domain.Robot result;

        protected static Domain.Robot robot;
    }

    public class when_asked_to_create_a_robot : robot_concern
    {
        private Because b = () => result = sut.Create();

        private It should_create_a_robot = () => result.ShouldNotBeNull();
    }

    public class when_asked_to_get_existing_robot : robot_concern
    {
        private Because b = () => result = sut.Get();

        private It should_return_existing_robot = () => result.ShouldNotBeNull();
    }

    public class when_asked_to_update_robot_with_location : robot_concern
    {
        private Establish c = () =>
            {
                robot = new Domain.Robot {Location = new Location {PointX = 3, PointY = 3}, Heading = Heading.North};
            };

        private Because b = () => result = sut.Update(robot);

        private It should_update_robot_point_x = () => robot.Location.PointX.ShouldEqual(3);

        private It should_update_robot_point_y = () => robot.Location.PointX.ShouldEqual(3);

        private It should_update_robot_heading = () => robot.Heading.ShouldEqual(Heading.North);

    }
}
