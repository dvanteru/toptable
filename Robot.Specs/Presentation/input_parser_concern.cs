﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using Robot.Presentation;
using Robot.Presentation.Parser;
using Robot.Presentation.Parser.Contract;
using Robot.Presentation.RobotInstructor.ViewModel;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Specs.Presentation 
{
    public abstract class input_parser_concern : Observes<IInputParser, InputParser>
    {
        private Establish c = () =>
            {
                inputString = new string[] { "5 5", "3 3 N LMLMLMLM" };
            };

        protected static RobotInputViewModel result;
        protected static string[] inputString;
    }

    public class when_asked_to_map_arena_input_to_viewmodel : input_parser_concern
    {
        private Because b = () => result = sut.Parse(inputString);
        private It should_return_robot_input_viewmodel_with_pointx = () => result.ArenaCoordinates.PointX.ShouldEqual(5);
        private It should_return_robot_input_viewmodel_with_pointy = () => result.ArenaCoordinates.PointY.ShouldEqual(5);
    }

    public class when_asked_to_map_arena_with_wrong_arguments : input_parser_concern
    {
        private Establish c = () =>
        {
            inputString = new string[] { "5 5 5", "3 3 N LMLMLMLM" };
        };

        private Because b = () => spec.catch_exception(() => sut.Parse(inputString));

        private It should_throw_invalid_data_exception =
            () => spec.exception_thrown.ShouldBeOfType<ArgumentException>();
    }

    public class when_asked_to_map_arena_with_alphabet_arguments : input_parser_concern {
        private Establish c = () =>
        {
            inputString = new string[] { "a b", "3 3 N LMLMLMLM" };
        };

        private Because b = () =>  spec.catch_exception(() => sut.Parse(inputString));

        private It should_throw_invalid_format_exception = () => spec.exception_thrown.ShouldBeOfType<FormatException>();
    }

    public class when_asked_to_map_robot_with_invalid_data : input_parser_concern {
        private Establish c = () =>
        {
            inputString = new string[] { "5 5", "3 MLMLMLM" };
        };

        private Because b = () => spec.catch_exception(() => sut.Parse(inputString));

        private It should_throw_invalid_argument_exception = () => spec.exception_thrown.ShouldBeOfType<ArgumentException>();

    }

    public class when_asked_to_map_robot_with_invalid_move_data : input_parser_concern
    {

        private Establish c = () =>
        {
            inputString = new string[] { "5 5", "3 3 N LDFMDFLMLMLM" };
        };

        private Because b = () => spec.catch_exception(() => sut.Parse(inputString));

        private It should_throw_invalid_argument_exception = () => spec.exception_thrown.ShouldBeOfType<ArgumentException>();
    }


    public class when_asked_to_map_robot_viewmodel_with_valid_data : input_parser_concern
    {
        private Because b = () => result = sut.Parse(inputString);
        private It should_return_robot_input_viewmodel_with_robot_at_pointx = () => result.RobotInstructions.First().RobotCoordinates.PointX.ShouldEqual(3);
        private It should_return_robot_input_viewmodel_with_robot_at_pointy = () => result.RobotInstructions.First().RobotCoordinates.PointY.ShouldEqual(3);

        private It should_return_robot_view_model_with_robot_heading_north =
            () => result.RobotInstructions.First().Heading.ShouldEqual("N");

        private It should_return_robot_viewmodel_with_spin_and_moves_LMLMLMLM = () =>
                                                                                result.RobotInstructions.First()
                                                                                      .SpinAndMoves.ShouldEqual(
                                                                                          "LMLMLMLM");

    }


    public class when_asked_to_map_robot_viewmodel_with_multiple_robots : input_parser_concern
    {
        private Establish c = () =>
        {
            inputString = new string[] { "5 5", "3 3 N LMLMLMLM", "4 4 E LRLRLRLRM" };
        };
        private Because b = () => result = sut.Parse(inputString);

        private It should_return_robot_viewmodel_with_2_robot_instructions =
            () => result.RobotInstructions.Count().ShouldEqual(2);
    }
}
