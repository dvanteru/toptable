﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using Robot.Domain;
using Robot.Presentation.Parser.Contract;
using Robot.Presentation.RobotInstructor;
using Robot.Presentation.RobotInstructor.ViewModel;
using Robot.Presentation.RobotInstructor.Contract;
using Robot.Tasks.Arena.Contract;
using Robot.Tasks.Robot.Contract;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Specs.Presentation 
{
    public abstract class robot_instructor_concern : Observes<IRobotInstructor, RobotInstructor>
    {
        private Establish context = () =>
            {
		args = new string[]{"5 5", "3 3 N LMLMLMLMM"};
                result = new List<string>();
		the_robot_positions = new List<string>();
                the_robot_viewmodel = new RobotInputViewModel();
                the_robot_viewmodel.ArenaCoordinates = new Coordinates{ PointX = 5, PointY = 5 };
                the_robot_viewmodel.RobotInstructions = new List<RobotInstruction>
                    {
                        new RobotInstruction
                            {
                                Heading = "N",
                                RobotCoordinates = new Coordinates{ PointX = 3, PointY = 3},
                                SpinAndMoves = "LMLMLMLLMRM"
                            }
                    };
                the_robot = new Domain.Robot {Heading = Heading.North, Location = new Location {PointX = 3, PointY = 3}};
                the_arena = new Domain.Arena {Location = new Location {PointX = 5, PointY = 5}};
                arenaTasks = depends.on<IArenaTasks>();                
                inputParser = depends.on<IInputParser>();
                robotHandler = depends.@on<IRobotHandler>();
                inputParser.setup(x => x.Parse(args)).Return(the_robot_viewmodel);

                arenaTasks.setup(x => x.CreateArena(the_robot_viewmodel.ArenaCoordinates.PointX.Value,
                                                                                   the_robot_viewmodel.ArenaCoordinates.PointY.Value)).Return((Arena) the_arena);
                robotHandler.setup(x => x.Handle(the_robot_viewmodel, the_arena, out the_robot_positions));

            };

        protected static List<string> result;
        protected static RobotInputViewModel the_robot_viewmodel;
        protected static IArenaTasks arenaTasks;
        protected static IRobotTasks robotTasks;
        protected static Arena the_arena;
        protected static Domain.Robot the_robot;
        protected static IInputParser inputParser;
        protected static string[] args;
        protected static IRobotHandler robotHandler;
        protected static List<string> the_robot_positions;
    }

    public class when_asked_to_instruct_robot_with_robot_input_viewmodel : robot_instructor_concern
    {
        private Because b = () => result = sut.Instruct(args);

        private It should_ask_arena_tasks_to_create_arena =
            () =>
            arenaTasks.received(
                x =>
                x.CreateArena(the_robot_viewmodel.ArenaCoordinates.PointX.Value,
                              the_robot_viewmodel.ArenaCoordinates.PointY.Value));

        private It should_ask_robot_handler_to_handle_robot =
            () => robotHandler.received(x => x.Handle(the_robot_viewmodel, the_arena, out the_robot_positions));
    }
}
