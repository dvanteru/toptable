﻿using Machine.Specifications;
using Robot.Infrastructure.Arena.Contract;
using Robot.Tasks.Arena;
using Robot.Tasks.Arena.Contract;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Specs.Tasks 
{
    [Subject("Arena Taks")]
    public abstract class arena_tasks_concern : Observes<IArenaTasks, ArenaTasks>
    {
    }

    public class when_asked_to_create_arena_with_location : arena_tasks_concern
    {
        private Establish c = () =>
            {
                pointX = 5;
                pointY = 5;
                arenaRepository = depends.on<IArenaRepository>();
		arenaRepository.setup(x => x.Get()).Return(new Domain.Arena());
            };
        private Because b = () => result = sut.CreateArena(pointX, pointY);

        private It should_ask_arena_repository_to_get_existing_arena = () => arenaRepository.received(x => x.Get());
        
        private It should_create_arena_with_pointx_5 = () => result.Location.PointX.ShouldEqual(5);

        private It should_create_arena_with_pointy_5 = () => result.Location.PointX.ShouldEqual(5);

        private static Domain.Arena result;
        private static int pointX;
        private static int pointY;
        private static IArenaRepository arenaRepository;
    }
}
