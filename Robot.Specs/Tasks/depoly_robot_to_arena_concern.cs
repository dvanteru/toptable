﻿using Machine.Specifications;
using Robot.Domain;
using Robot.Infrastructure.Arena.Contract;
using Robot.Infrastructure.Robot.Contract;
using Robot.Tasks.Robot;
using Robot.Tasks.Robot.Contract;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Specs.Tasks
{
    [Subject("Deploy Robot to Arena Concern")]
    public abstract class depoly_robot_to_arena_concern : Observes<IRobotTasks, RobotTasks>
    {
        protected static Domain.Robot result;
        protected static int pointX;
        protected static int pointY;
        protected static string heading;
    }

    public class when_asked_to_deploy_robot : depoly_robot_to_arena_concern
    {
        private Establish c = () =>
            {
                pointX = 5;
                pointY = 4;
                heading = "N";
                robotRepository = depends.on<IRobotRepository>();
                robot = new Domain.Robot();
                robotRepository.setup(x => x.Create()).Return(robot);
            };

        private Because b = () => result = sut.DeployRobot(pointX, pointY, heading);

        private It shoud_ask_robot_repository_to_create_robot = () => robotRepository.received(x => x.Create());

        private It should_deploy_robot_at_postion_x = () => robot.Location.PointX.ShouldEqual(5);

        private It should_deploy_robot_at_position_y = () => robot.Location.PointY.ShouldEqual(4);

        private It should_deploy_robot_heading_north = () => robot.Heading.ShouldEqual(Heading.North);

        private It should_deploy_robot_heading_north_with_left_side_west = () => robot.Heading.LeftHeading.ShouldEqual("W");

        private static IRobotRepository robotRepository;
        private static Domain.Robot robot;
    }



}