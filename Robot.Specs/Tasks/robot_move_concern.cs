﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using Robot.Domain;
using Robot.Infrastructure.Robot.Contract;
using Robot.Tasks.Robot;
using Robot.Tasks.Robot.Contract;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Specs.Tasks 
{
    [Subject("Robot Move Concern")]
    public abstract class robot_move_concern  : Observes<IRobotTasks, RobotTasks>
    {
        protected static Domain.Robot result;
        protected static IRobotRepository robotRepository;
        protected static int pointX;
        protected static int pointY;
        protected static Domain.Robot robot;

        private Establish c = () =>
            {

                pointX = 1;
                pointY = 2;
                robot = new Domain.Robot
                    {
                        Heading = Heading.North,
                        Location = new Location
                            {
                                PointX = pointX, 
                                PointY = pointY
                            }
                    };
                robotRepository = depends.@on<IRobotRepository>();
                robotRepository.setup(x => x.Get()).Return(robot);
                robotRepository.setup(x => x.Update(robot)).Return(robot);
            };
    }

    public class when_asked_to_move_heading_north : robot_move_concern
    {
        private Because b = () => result = sut.Move();

        private It should_ask_robot_repository_to_get_current_robot = () => robotRepository.received(x => x.Get());

        private It should_increment_robot_location_y_by_1 = () => result.Location.PointY.ShouldEqual(3);

        private It should_ask_robot_repository_to_update_robot = () => robotRepository.received(x => x.Update(robot));
    }

    public class when_asked_to_move_heading_south : robot_move_concern
    {
        private Establish c = () => robot.Heading = Heading.South;
        private Because b = () => result = sut.Move();
        private It should_decrement_point_y_by_1 = () => result.Location.PointY.ShouldEqual(1);
    }

    public class when_asked_to_move_heading_east : robot_move_concern
    {
        private Establish c = () => robot.Heading = Heading.East;
        private Because b = () => result = sut.Move();
        private It should_increment_point_x_by_1 = () => result.Location.PointX.ShouldEqual(2);        
    }

    public class when_asked_to_move_heading_west : robot_move_concern
    {
        private Establish c = () => robot.Heading = Heading.West;
        private Because b = () => result = sut.Move();
        private It should_decrement_point_x_by_1 = () => result.Location.PointX.ShouldEqual(0);
    }
}
