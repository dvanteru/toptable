﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using Robot.Domain;
using Robot.Infrastructure.Robot.Contract;
using Robot.Tasks.Robot;
using Robot.Tasks.Robot.Contract;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace Robot.Specs.Tasks 
{
    [Subject("Robot Spin Concern")]
    public abstract class robot_spin_concern : Observes<IRobotTasks, RobotTasks>
    {
        private Establish c = () =>
            {
                robot = new Domain.Robot{ Heading = Heading.North};
                robotRepository = depends.on<IRobotRepository>();
                robotRepository.setup(x => x.Get()).Return((Domain.Robot) robot);
                robotRepository.setup(x => x.Update(robot)).Return((Domain.Robot) robot);
            };

        protected static IRobotRepository robotRepository;
        private static Domain.Robot robot;
        protected static Domain.Robot result;
    }

    public class when_asked_to_spin_left : robot_spin_concern
    {
        private Because b = () => result = sut.Spin(SpinDirection.Left.Key);

        private It should_rotate_robot_90_degrees_left_head_west = () => result.Heading.ShouldEqual(Heading.West);

        private It should_ask_robot_repository_to_get_current_robot = () => robotRepository.received(x => x.Get());
    }

    public class when_asked_to_spin_right : robot_spin_concern
    {
        private Because b = () => result = sut.Spin(SpinDirection.Right.Key);

        private It should_rotate_robot_90_degrees_right_head_east = () => result.Heading.ShouldEqual(Heading.East);
    }

}
