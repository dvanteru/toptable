﻿using Robot.Domain;
using Robot.Infrastructure.Arena.Contract;
using Robot.Tasks.Arena.Contract;

namespace Robot.Tasks.Arena
{
    public class ArenaTasks : IArenaTasks
    {
        private readonly IArenaRepository arenaRepository;

        public ArenaTasks(IArenaRepository arenaRepository)
        {
            this.arenaRepository = arenaRepository;
        }

        public Domain.Arena CreateArena(int pointX, int pointY)
        {
            var arena = arenaRepository.Get();
            arena.Location = new Location{ PointX = pointX, PointY = pointY};
            return arena;
        }
    }
}