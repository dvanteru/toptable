namespace Robot.Tasks.Arena.Contract
{
    public interface IArenaTasks
    {
        Domain.Arena CreateArena(int pointX, int pointY);
    }
}