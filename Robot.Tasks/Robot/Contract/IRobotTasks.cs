using Robot.Domain;

namespace Robot.Tasks.Robot.Contract
{
    public interface IRobotTasks
    {
        Domain.Robot DeployRobot(int pointX, int pointY, string heading);
        Domain.Robot Spin(string left);
        Domain.Robot Move();
    }
}