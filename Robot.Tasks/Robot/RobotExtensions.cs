using Robot.Domain;

namespace Robot.Tasks.Robot
{
    public static class RobotExtensions
    {
        public static Domain.Robot Spin(this Domain.Robot robot, SpinDirection spinDirection)
        {
            if (spinDirection == SpinDirection.Left)
                robot.Heading = Heading.GetFor(robot.Heading.LeftHeading);
            else if(spinDirection == SpinDirection.Right) 
            robot.Heading = Heading.GetFor(robot.Heading.RightHeading);
            return robot;
        }
    }
}