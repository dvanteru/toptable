using Robot.Domain;
using Robot.Infrastructure.Arena.Contract;
using Robot.Infrastructure.Robot.Contract;
using Robot.Tasks.Robot.Contract;

namespace Robot.Tasks.Robot
{
    public class RobotTasks : IRobotTasks
    {
        private readonly IArenaRepository arenaRepository;

        private readonly IRobotRepository robotRepository;

        public RobotTasks(IArenaRepository arenaRepository, IRobotRepository robotRepository)
        {
            this.arenaRepository = arenaRepository;
            this.robotRepository = robotRepository;
        }

        public Domain.Robot DeployRobot(int pointX, int pointY, string heading)
        {
            var robot = robotRepository.Create();
	    robot.Location = new Location { PointX = pointX, PointY =  pointY};
            robot.Heading = Heading.GetFor(heading);
            return robot;
        }

        public Domain.Robot Spin(string spinDirection)
        {
            var robot = robotRepository.Get();
            return robotRepository.Update(robot.Spin(SpinDirection.GetFor(spinDirection)));
        }
        
	//TODO: Refactor move increment values to Heading enum.
        public Domain.Robot Move()
        {
            var robot = robotRepository.Get();
            robot.Heading.RobotMove(robot);
            return robotRepository.Update(robot);
        }
    }
}